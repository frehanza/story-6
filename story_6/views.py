from django.shortcuts import render, redirect
from django.http import HttpResponseBadRequest

# Models
from .models import Kegiatan, Person

def index(request):
  context = {}
  context['daftar_kegiatan'] = Kegiatan.objects.all()
  return render(request, 'story_6/index.html', context)

def add_kegiatan(request):
  if (request.method == 'POST'):
    try:
      kegiatan_baru = Kegiatan(name=request.POST['kegiatan_name'])
      kegiatan_baru.save()
    except KeyError:
      return HttpResponseBadRequest('Kegiatan instance not found')
  return redirect('home')

def delete_kegiatan(request, pk):
  try:
      kegiatan = Kegiatan.objects.get(pk=pk)
  except Kegiatan.DoesNotExist:
    return HttpResponseBadRequest('Kegiatan instance not found')

  kegiatan.delete()
  return redirect('home')

def add_peserta(request, pk=None):
  context = {}
  if (request.method == 'POST'):
    # Check if kegiatan does not exist
    try:
      kegiatan = Kegiatan.objects.get(pk=pk)
    except Kegiatan.DoesNotExist:
      return HttpResponseBadRequest('Kegiatan instance not found')

    # Check if data is sent with post request
    try:
      name = request.POST['nama_peserta']
    except KeyError:
      return HttpResponseBadRequest('Peserta name not found')

    person = Person(name=name, kegiatan=kegiatan)
    person.save()
    return redirect(f'/daftar-peserta/{kegiatan.pk}/')
  else:
    return HttpResponseBadRequest('GET not allowed')


def daftar_peserta(request, pk=None):
  context = {}
  try:
    kegiatan = Kegiatan.objects.get(pk=pk)
    context['kegiatan'] = kegiatan
    return render(request, 'story_6/daftar-peserta.html', context)
  except Kegiatan.DoesNotExist:
    return HttpResponseBadRequest('Kegiatan instance not found')



